#! /usr/bin/python3
# ! coding: utf-8

from tkinter import *
from tkinter import messagebox
from tkinter import ttk
import ftplib
import os
import zipfile
import configparser
from subprocess import Popen, PIPE
import time
import logging
import shutil
import sys

path_for_this_script = os.getcwd()
ini_file_upd_comments = 'upd_comments.ini'
ini_file_settings = 'Settings.ini'
config = configparser.ConfigParser()
# ftp_host = "192.168.100.106"
# ftp_user = "ps"
ftp_path = "Update/updGlobal/upd"
ftp_pass = "*#psftp"
dir_upd = 'Updates'
time = time.strftime('%d-%m-%y''___''%X')
stdout_log = ''
errors = 'errors.log'

if not os.path.exists('Log'):
    os.mkdir('Log')
logging.basicConfig(format=u'[LINE:%(lineno)d]# %(levelname)-8s[%(asctime)s] %(message)s',
                    level=logging.INFO, filename=u'Log/update{}.log'.format(time))


def read_settings(file):
    config.read(file)
    ftp_host = config.get('Global', 'ftp_host')
    ftp_user = config.get('Global', 'ftp_user')
    return ftplib.FTP(ftp_host, ftp_user, ftp_pass)


def select_close():
    try:
        os.remove(ini_file_upd_comments)
        logging.info('Close programm')
        sys.exit()
    except Exception as err:
        messagebox.showwarning('Ошибка при закрытии программы', "{}".format(err))
        logging.error('Error \n {}'.format(err))


def read_list_upd():
    """This is function connects to ftp server, reads list
     of available updates for installation,
     and displays the user in a listbox for selection."""
    try:
        connect_ftp = read_settings(ini_file_settings)
        connect_ftp.encoding = 'UTF-8'
        connect_ftp.cwd(ftp_path)
        logging.info('Connect to ftp')
        with open(ini_file_upd_comments, 'wb') as comments_file:
            response = connect_ftp.retrbinary('RETR ' + ini_file_upd_comments, comments_file.write)
        list_ftp = connect_ftp.nlst()
        listbox.delete(0, END)
        logging.info('Clear list updates in window')
        config.read(ini_file_upd_comments)
        logging.info('Read updates in server')
        for line in list_ftp:
            if 'upd' in line and config.has_option('Global', line):
                comment_upd = config.get('Global', line)
                view_string = line.strip(' \n\t\r '), '=', comment_upd
                listbox.insert(END, view_string)
                logging.info('{}'.format(view_string))
        connect_ftp.close()
    except ftplib.all_errors as err:
        logging.error('Error \n {}'.format(err))
        messagebox.showwarning('Ошибка при подключении к FTP серверу', "{}".format(err))


def download_upd():
    """This is function connects to ftp server, download and installation UPD, select user."""

    global name
    if listbox.curselection():
        value = listbox.curselection()
        name = listbox.get(value)

    def install_upd():

        """ This is function installation UPD, select user. """
        progress_bar['value'] = 10
        progress_bar.update()

        dir_name = name_for_download.split('.')[0]
        logging.info('Start install UPD {}.'.format(dir_name))

        if not os.path.exists(dir_name):
            os.makedirs(dir_name)
            logging.info('Create dir {}.'.format(dir_name))

        progress_bar['value'] = 20
        progress_bar.update()

        try:
            with zipfile.ZipFile(name_for_download, 'r') as result_zip:
                logging.info('Open Zip {}.'.format(name_for_download))
                result_zip.extractall(dir_name)
                result_zip.close()

            progress_bar['value'] = 35
            progress_bar.update()
            logging.info('Zip extract to  {}.'.format(dir_name))

            if os.path.exists("{}/{}".format(dir_name, dir_name)):
                os.chdir(dir_name)

            for file in os.listdir(dir_name):
                os.chmod('{}/{}'.format(dir_name, file), 0o0777)

            progress_bar['value'] = 50
            progress_bar.update()

            logging.info('Chmod +x files in {} .'.format(dir_name))
            os.chdir(dir_name)
            logging.info('Started UPD {}.sh.'.format(dir_name))
            start_upd = Popen('./{}.sh'.format(dir_name), shell=True, stdout=PIPE, stderr=PIPE)

            progress_bar['value'] = 65
            progress_bar.update()
            start_upd.wait()
            std_out_log = start_upd.communicate()[1]
            std_out_log = std_out_log.decode()

            if os.path.exists(errors):
                logging.error('Error install UPD {}.sh, show message Error form user.'.format(dir_name))
                shutil.copy(errors, path_for_this_script)
                raise SystemExit

            if start_upd.returncode != 0:
                logging.error('Error install UPD {}.sh, show message Error form user.'.format(dir_name))
                raise SystemExit

            progress_bar['value'] = 80
            progress_bar.update()
            logging.info('Remove {}, {} .'.format(dir_name, name_for_download))
            progress_bar['value'] = 100
            progress_bar.update()

            messagebox.showwarning('Результат обновления.', 'Обновление {} установлено успешно.'
                                   .format(name_for_download))

        except zipfile.BadZipFile as err:
            messagebox.showwarning('Ошибка', 'Не удалось обработать архив {}, попробуйте еще раз.\n {}'
                                   .format(name_for_download, err))
            logging.info('Error \n {}.'.format(err))
            os.remove(name_for_download)
            os.remove(dir_name)

        finally:
            progress_bar['value'] = 0
            progress_bar.update()

    try:
        if listbox.curselection():
            value = listbox.curselection()
            name = listbox.get(value)
            logging.info('{} selected, start download'.format(name))
            connect_ftp = read_settings(ini_file_settings)
            connect_ftp.encoding = 'UTF-8'
            connect_ftp.cwd('Update/updGlobal/upd')
            logging.info('Connect to ftp')
            name_for_download = name[0]
            if not os.path.exists(dir_upd):
                os.makedirs(dir_upd)
                logging.info('Make dir {}.'.format(dir_upd))
            os.chdir(dir_upd)
            with open(name[0], 'wb') as upd_file:
                response = connect_ftp.retrbinary('RETR ' + name[0], upd_file.write)
                logging.info('Response ftp {}.'.format(response))
                if not response.startswith("226 Successfully transferred "):
                    os.remove(name[0])
                    logging.info('Remove {}, show message error.'.format(name[0]))
                    messagebox.showwarning('Ошибка',
                                           'Не удалось скачать обновление {}, попробуйте еще раз.'.format(name[0]))
                    upd_file.close()
                else:
                    upd_file.close()
                    logging.info('Close {} .'.format(name[0]))
                    upd_file.close()
                    install_upd()
        else:
            messagebox.showwarning('Ошибка при установке обновления', "Не выбрано обновление для загрузки!")
    except Exception as err:
        messagebox.showwarning('Ошибка при установке обновления', "{}".format(err))
        logging.error('Error \n {}.'.format(err))
    except SystemExit:
        messagebox.showwarning('Ошибка', 'Ошибка при установке обновления')
        window_error = Toplevel()
        window_error.title("Протокол установки")
        window_error.wm_geometry("%dx%d+%d+%d" % (850, 700, 0, 0))
        error_box = Listbox(window_error, width=90, height=35, yscrollcommand=scroll.set, font=('Courier', 11),
                            selectbackground='light blue')
        error_box.place(x=10, y=10)
        for line in stdout_log.split("\n"):
            error_box.insert(END, line.strip(" \n\t\r "))
            logging.error('Error \n {}.'.format(line))
        for line in open(errors, 'r', encoding='UTF-8'):
            error_box.insert(END, line.strip(" \n\t\r "))
            logging.error('Error \n {}.'.format(line))
    finally:
        os.chdir(path_for_this_script + "/Updates")
        os.remove(name[0])
        shutil.rmtree(name[0].split('.')[0])
        os.chdir(path_for_this_script)


def auto_install():
    std_out_log = ''
    logging.info('User chose Auto_install.')
    try:
        start_upd = Popen('./Auto_search_task.py', shell=True, stdout=PIPE, stderr=PIPE)
        start_upd.wait()

        std_out_log = start_upd.communicate()[1]
        std_out_log = std_out_log.decode()

        if not start_upd.returncode == 0 or os.path.exists(errors):
            logging.error('Error auto-install UPD show message Error form user.')
            raise SystemExit
        messagebox.showwarning('Результат обновления.', 'Автоматический поиск и установка\n      завершена успешно.')
        logging.info('Successfully Auto_install')
    except SystemExit:
        messagebox.showwarning('Ошибка при установке обновления', "{}".format(std_out_log))
        logging.error('Error Auto_install {}.'.format(std_out_log))
        os.system("gedit {}".format(errors))


def get_help():
    reference = '''Данная программа предназначена для просмотра \n
    и установки доступных обнавлений. Кассовой программы и оборудования.\n 
    Принцип работы:\n
    1. Выбрать из списка доступных обновлений.\n
    2. Нажать на кнопку Скачать и установить обновление\n
    3. Ждать сообщения о результате установке '''
    window_help = Toplevel()
    window_help.title("Справка")
    window_help.wm_geometry("%dx%d+%d+%d" % (625, 330, 0, 0))
    window_help = Label(window_help, text=reference, width=75, height=15, font=('Times', 12), bg='white')
    window_help.place(x=10, y=10)


root = Tk()
root.title("Администратор обновлений")
root.wm_geometry("%dx%d+%d+%d" % (1355, 650, 0, 0))
frame = Frame(root)
update_list_upd_button = Button(root, text="Обновить список доступных обновлений", command=read_list_upd,
                                activebackground='light blue', relief=GROOVE, fg="black", font='times 11')
update_list_upd_button.place(relx=0.874, rely=0.04, anchor="c")
update_list_upd_button.config(width=39)
download_upd_button = Button(root, text="Скачать и установить обновление", command=download_upd,
                             activebackground='light blue', relief=GROOVE, fg="black", font='times 11')
download_upd_button.place(relx=0.874, rely=0.1, anchor="c")
download_upd_button.config(width=39)
progress_bar = ttk.Progressbar(root, value=0, orient="horizontal", mode="determinate", length=300)
progress_bar.place(relx=0.874, rely=0.16, anchor="c")
auto_upd_button = Button(root, text="Автоматический поиск и установка\n обновлнеий", command=auto_install,
                         relief=GROOVE, activebackground='light blue', font='times 11')
auto_upd_button.place(relx=0.874, rely=0.22, anchor="c")
auto_upd_button.config(width=39)
help_button = Button(root, text="Справка", command=get_help, activebackground='light blue',
                     relief=GROOVE, fg="black", font='times 11')
help_button.place(relx=0.81, rely=0.96, anchor="c")
help_button.config(width=15)
btn_close = Button(root, text='Выход', relief=GROOVE, command=select_close,
                   activeforeground='white', activebackground='#b20101', font='times 11')
btn_close.place(relx=0.94, rely=0.96, anchor="c")
btn_close.config(width=15)
scroll = Scrollbar(root)
scroll.pack(side=LEFT, fill=Y)
listbox = Listbox(root, width=105, height=33, yscrollcommand=scroll.set, font=('Courier', 11),
                  selectbackground='light blue')
listbox.place(x=14, y=10)
scroll.config(command=listbox.yview)
edit_menu = Menu(listbox, tearoff=0)
edit_menu.add_command(label="Cut", accelerator="Ctrl+X", command=lambda: listbox.event_generate('<<Cut>>'))
edit_menu.add_command(label="Copy", accelerator="Ctrl+C", command=lambda: listbox.event_generate('<<Copy>>'))
edit_menu.add_command(label="Paste", accelerator="Ctrl+V", command=lambda: listbox.event_generate('<<Paste>>'))
listbox.bind("<Button-3>", lambda event: edit_menu.post(event.x_root, event.y_root))

read_list_upd()

root.mainloop()

#! coding: UTF-8

import shutil
import sys
import os

file_admin_upd = 'Main.py'
file_auto_upd = 'Auto_search_task.py'
file_settings = 'Settings.ini'
file_auto_start = 'auto_start.sh'
file_auto_window = 'start_window.sh'
path_admin_upd = "/home/cashier/scripts/AdminUPD"


def copy_file(name_file, path):
    try:
        if not os.path.exists(path):
            os.system('mkdir {}'.format(path))
        shutil.copy(name_file, path)
    except Exception as error:
        print("Ошибка при копировании файлов\n {}".format(error))
        sys.exit(1)


copy_file(file_admin_upd, path_admin_upd)
copy_file(file_auto_upd, path_admin_upd)
copy_file(file_settings, path_admin_upd)
copy_file(file_auto_start, path_admin_upd)
copy_file(file_auto_window, path_admin_upd)

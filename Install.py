#! /usr/bin/python
# ! coding: utf-8

import os
import logging

bash_rc = '/home/cashier/.bashrc'
search_line = '# Alias definitions.'
path_admin_upd = "/home/cashier/scripts/AdminUPD"
insert_line = "alias admin_upd='~/scripts/AdminUPD/start_window.sh'"
insert_line_2 = "alias auto_upd='~/scripts/AdminUPD/auto_start.sh'"

if not os.path.exists(path_admin_upd):
    os.makedirs(path_admin_upd)

new_line = ''
check_line = 0

file_bash_rc = open(bash_rc, 'r', encoding='UTF-8')
if insert_line in file_bash_rc.read():
    check_line = 1
file_bash_rc.close()

if check_line == 0:
    file_bash_rc = open(bash_rc, 'r', encoding='UTF-8')
    for line in file_bash_rc:
        if not line.find(search_line) == -1:
            line = "{}\n{}\n{}\n".format(insert_line, insert_line_2, line)
            logging.info("Добавлены строки в bashrc {}\n{}".format(insert_line, insert_line_2))
        new_line += line
    file_bash_rc.close()

    new_file_bash_rc = open(bash_rc, 'w', encoding='UTF-8')
    new_file_bash_rc.write(new_line)
    new_file_bash_rc.close()

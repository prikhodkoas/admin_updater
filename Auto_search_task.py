#! /usr/bin/python3

import ftplib
import configparser
import re
import os
import zipfile
import shutil
import time
import logging
from subprocess import Popen, PIPE

ini_file_settings = 'Settings.ini'
path_for_this_script = os.getcwd()
ini_file_cash_main = '/home/cashier/.wine/drive_c/windows/CashMain.ini'
term_line = 'TerninalNumber'
time_upload_log = time.strftime('%d-%m-%y')
errors = 'errors.log'
time = time.strftime('%d-%m-%y''___''%X')


if not os.path.exists('Log'):
    os.mkdir('Log')
logging.basicConfig(format=u'[LINE:%(lineno)d]# %(levelname)-8s[%(asctime)s] %(message)s',
                    level=logging.INFO, filename=u'Log/Auto-update{}.log'.format(time))


class SearchFtp:

    def __init__(self, ftp_path, connect_ftp=None):
        self.ftp_path = ftp_path
        self.connect_ftp = connect_ftp
        config = configparser.ConfigParser(strict=False)
        config.read(ini_file_settings, encoding='UTF-8')
        self.key_ftp_host = config.get('Global', 'ftp_host')
        self.key_ftp_user = config.get('Global', 'ftp_user')
        self.ftp_pass = "*#psftp"
        connect_ftp = ftplib.FTP(self.key_ftp_host, self.key_ftp_user, self.ftp_pass)
        connect_ftp.encoding = 'UTF-8'
        connect_ftp.cwd(ftp_path)
        self.path_list = connect_ftp.nlst()
        connect_ftp.close()

    @staticmethod
    def read_ini(file, coding):
        config = configparser.ConfigParser(strict=False)
        config.read(file, encoding=coding)
        return config

    @staticmethod
    def install_upd(name):
        """This is function installation UPD, select user."""
        dir_name = name
        stdout_log = ''
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)
        try:
            with zipfile.ZipFile('{}.zip'.format(dir_name), 'r') as result_zip:
                logging.info('Open Zip {}.zip.'.format(dir_name))
                print('Open Zip {}.zip.'.format(dir_name))
                result_zip.extractall(dir_name)
                result_zip.close()
                logging.info('Successfully unzip: {}.zip'.format(dir_name))
                print('Successfully unzip: {}.zip'.format(dir_name))
            if os.path.exists("{}/{}".format(dir_name, dir_name)):
                os.chdir(dir_name)
            for file in os.listdir(dir_name):
                os.chmod('{}/{}'.format(dir_name, file), 0o0777)
            os.chdir(dir_name)
            start_upd = Popen('./{}.sh'.format(dir_name), shell=True, stdout=PIPE, stderr=PIPE)
            start_upd.wait()
            stdout_log = start_upd.communicate()[1]
            stdout_log = stdout_log.decode()
            if not start_upd.returncode == 0:
                logging.error('Error install UPD {}.sh, show message Error form user.'.format(dir_name))
                print('Error install UPD {}.sh, show message Error form user.'.format(dir_name))
                raise SystemExit
            logging.info('Successfully install: {}.sh'.format(dir_name))
            print('Successfully install: {}.sh'.format(dir_name))
        except zipfile.BadZipFile as err:
            os.remove(name)
            os.remove(dir_name)
            print("-------Error-------\n{}".format(err))
            logging.error('STDERR: ==> {}'.format(err))
        except SystemExit:
            print("--Error Install upd {}--\n{}".format(dir_name, stdout_log))
            logging.error('STDERR: ==> {}'.format(stdout_log))

    def download_upd(self, name):
        self.connect_ftp = ftplib.FTP(self.key_ftp_host, self.key_ftp_user, self.ftp_pass)
        self.connect_ftp.encoding = 'UTF-8'
        self.connect_ftp.cwd(self.ftp_path)
        with open("{}.zip".format(name), 'wb') as upd_file:
            response = self.connect_ftp.retrbinary('RETR ' + "{}.zip".format(name), upd_file.write)
            upd_file.close()
            self.connect_ftp.close()
            if not response.startswith("226 Successfully transferred "):
                print("-------Error-------\n{}".format(response))
                if os.path.exists(errors):
                    shutil.copy(errors, path_for_this_script)
                    os.system('cat ./{}'.format(errors))
            else:
                logging.info('Successfully download task: {}'.format(name))
                print('Successfully download task: {}'.format(name))
                SearchFtp.install_upd(name)
                self.upload_log(name)
                os.chdir(path_for_this_script)
            shutil.rmtree(name)
            logging.info('Remove: {}'.format(name))
            print('Remove: {}'.format(name))
            os.remove("{}.zip".format(name))
            logging.info('Remove: {}.zip'.format(name))
            print('Remove: {}.zip'.format(name))

    def upload_log(self, name):
        log_name = 'update.log'
        log_path = "Update/updGlobal/log/{}".format(name)
        self.connect_ftp = ftplib.FTP(self.key_ftp_host, self.key_ftp_user, self.ftp_pass)
        self.connect_ftp.cwd(log_path)
        if os.path.exists(log_name):
            new_name = "({})({})({}).log".format(system_name, name, time_upload_log)
            shutil.copy(log_name, new_name)
            try:
                with open(new_name, 'rb') as file:
                    response_upload_log = self.connect_ftp.storlines("STOR " + new_name, file)
                    file.close()
                if not response_upload_log.startswith("226 Successfully transferred "):
                    with open(new_name, 'rb') as file:
                        self.connect_ftp.storlines("STOR " + new_name, file)
                        file.close()
            except Exception as err:
                logging.info('Error Upload log: {}'.format(err))

    def del_task(self, task):
        self.connect_ftp = ftplib.FTP(self.key_ftp_host, self.key_ftp_user, self.ftp_pass)
        self.connect_ftp.encoding = 'UTF-8'
        self.connect_ftp.cwd(self.ftp_path)
        self.connect_ftp.delete(task)
        self.connect_ftp.close()


def check_ini_file_on_duplicate_terminal_number(file_name):
    file = open(file_name, 'r', encoding='cp1251')
    check = 0
    new_file = ''
    for line in file:
        if not line.find(term_line) == -1:
            if line[0] != str(";") and check != 0:
                line = re.sub(line, ';{}'.format(line), line)
            check += 1
        new_file += line
    file.close()
    file = open(file_name, 'w', encoding='cp1251')
    file.write(new_file)
    file.close()


def search_and_download_task_in_ftp(name, list_task_ftp):
    check = 0
    for task in list_task_ftp:
        if task.split('_')[0] == name:
            check = 1
            logging.info('Found task  in Ftp server: {}'.format(task))
            print('Found task  in Ftp server: {}'.format(task))
            download_upd_class.download_upd(task.split('_')[1].split('.')[0])
            search_task.del_task(task)
            logging.info('Delete task in Ftp server: {}'.format(task))
            print('Delete task in Ftp server: {}'.format(task))
    if check == 0:
        print("Not found task in FTP server")


check_ini_file_on_duplicate_terminal_number(ini_file_cash_main)
system_name = SearchFtp.read_ini(ini_file_cash_main, 'cp1251').get('GLOBAL', 'TerninalNumber')
logging.info('Terminal Number ==> {}'.format(system_name))
print('Terminal Number ==> {}'.format(system_name))

search_upd = SearchFtp(ftp_path="Update/updGlobal/upd")
list_upd = search_upd.path_list
logging.info('ls from Update/updGlobal/upd :\n{}'.format(list_upd))

search_task = SearchFtp(ftp_path="Update/updGlobal/task")
list_task = search_task.path_list
logging.info('ls from Update/updGlobal/task :\n{}'.format(list_task))

download_upd_class = SearchFtp(ftp_path="Update/updGlobal/upd")

search_and_download_task_in_ftp(system_name, list_task)
